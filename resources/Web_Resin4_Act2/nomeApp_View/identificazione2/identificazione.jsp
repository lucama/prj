<%--@  Schermata d'identificazione   @--%>
<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij"%><%--
--%><%@page import="jaf.controller2.bundle.CodiciBundle"%><polij:useBean id="pwd_scaduta" className="java.lang.String" /><%--
--%><polij:useBean id="POLIJ_BOOT_CONTROLLER_TIME" className="java.util.Date" /><%--
--%><polij:useBean id="POLIJ_BOOT_CONTROLLER_SERVER" className="java.lang.String" /><%--
--%><polij:useBean id="POLIJ_BOOT_CONTROLLER_JARS" className="java.lang.String" /><%--
--%><polij:useBean id="INTERNATIONALIZER" className="jaf.model2.authentication.Internationalizer" /><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %><%--
--%><polij:useBean id="lastActivityURL" className="java.lang.String" />
<!--
  	##################################################################
	View start time: <%=request.getAttribute("POLIJ_BOOT_VIEW_TIME")%>
	*************
	View jars: <%=System.getProperty("jaf2.internal.webappConfig")%>
	***************
	Controller start time: <%=POLIJ_BOOT_CONTROLLER_TIME%>
	****************
	Controller server: <%=POLIJ_BOOT_CONTROLLER_SERVER%>
	***************
	Controller jars: <%=POLIJ_BOOT_CONTROLLER_JARS%>
  	##################################################################
-->
<%
	// niente invii
	response.resetBuffer();
%>
<style type="text/css">
	#passwordcell		{border-right: none;}
	td.ElementInfoCard1	{width: 11em;}
	td.TitleInfoCard	{width: 9em;}
	td.td-comandi		{width: 7em;}
	.input-credenziali	{width: 98%;}
	.info-utente		{display:none;}
    .imposta_pagina		{
    	padding:2em 0;
    	width: 50%;
    	min-width: 35em;
   }
</style>
<polij:page>
	<polij:centerbar  width="50%">
		<form name="" action="<polij:href><%=lastActivityURL%></polij:href>" method="post">
			<div class="imposta_pagina">
				<polij:infoCard title="<%=INTERNATIONALIZER.getText(CodiciBundle.JAF_TIT_IDENTIFICAZIONE)%>">
					<polij:row>
						<polij:element classElement="ElementInfoCard1" colspan="1" rowspan="1"><label><%=INTERNATIONALIZER.getText(CodiciBundle.JAF_LBL_MATRICOLA)%></label></polij:element>
						<polij:element classElement="ElementInfoCard2 jaf-no-border-right" colspan="1" rowspan="1"><input id="login" type="text" class="input-credenziali" name="polij_username" value="" placeholder="${I18N.JAF_PLACEHOLDER_MATRICOLA}"></polij:element>
						<polij:element classElement="ElementInfoCard2 td-comandi" colspan="1" rowspan="1"></polij:element>
					</polij:row>
					<polij:row>
						<polij:element classElement="ElementInfoCard1" colspan="1" rowspan="1"><label><%=INTERNATIONALIZER.getText(CodiciBundle.JAF_LBL_PWD)%></label></polij:element>
						<polij:element classElement="ElementInfoCard2" id="passwordcell" colspan="1" rowspan="1" ><input id="password" type="password" class="input-credenziali"  name="polij_pwd" value="" placeholder="${I18N.JAF_PLACEHOLDER_PWD}"></polij:element>
						<polij:element classElement="ElementInfoCard2 td-comandi" colspan="1" rowspan="1"><input type="submit" name="evn_conferma=evento" value="<%=INTERNATIONALIZER.getText(CodiciBundle.JAF_CMD_CONFERMA)%>"></polij:element>
					</polij:row>
				</polij:infoCard>
			</div>		
		</form>
<% jaf.view2.taglibs.ajax.JQuery.loadJQuery(pageContext,out); %>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#login').focus(
                function() {
                    $("#helplogin").css("background-color","lightyellow");
                    if (jQuery.browser.msie) { $("#login").css("background-color","lightyellow") };
                }
            );
            $('#login').blur(
                function() {
                    $("#helplogin").css("background-color","white");
                    if (jQuery.browser.msie) { $("#login").css("background-color","white") };
                }
            );
            $('#password').focus(
                function() {
                    $("#helppassword").css("background-color","lightyellow");
                    if (jQuery.browser.msie) { $("#password").css("background-color","lightyellow") };
                }
            );
            $('#password').blur(
                function() {
                    $("#helppassword").css("background-color","white");
                }
            );
            // on focus per IE 6 / 7
            if (jQuery.browser.msie) { 
                $('#login').focus(
                    function() {
                        $("#login").css("background-color","lightyellow");
                        $("#login").css("border","1px solid gray");
                    }
                );
                $('#login').blur(
                    function() {
                        $("#login").css("background-color","white");
                    }
                );
                $('#password').focus(
                    function() {
                        $("#password").css("background-color","lightyellow");
                        $("#password").css("border","1px solid gray");
                    }
                );
                $('#password').blur(
                    function() {
                        $("#password").css("background-color","white");
                    }
                );
            }
            $('#login').focus();			
        });
    </script>
	</polij:centerbar>
</polij:page>
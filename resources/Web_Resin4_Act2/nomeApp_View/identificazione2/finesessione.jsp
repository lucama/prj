<%@page import="jaf.common.Configuration"%>
<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij" %>
<div style="padding:2em 3em;">
	<strong class="jaf-font-size-grandi">Sessione terminata.</strong><br/><br/>
	<%if(Configuration.isSviluppoMode())%>
	<a href="<%=request.getContextPath()%>/"><strong class="jaf-font-size-grandi">Nuova sessione</strong></a><br/>
	<%session.invalidate();%>
</div>
<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij"%><%--
--%><%@page import="jaf.controller2.bundle.CodiciBundle"%><%--
--%><polij:useBean id="lastActivityURL" className="java.lang.String" /><%--
--%><polij:useBean id="INTERNATIONALIZER" className="jaf.model2.authentication.Internationalizer" /><%--
--%><polij:useBean id="polij_elenco_abilitazioni" className="jaf.model2.DataObjectCollection" /><%--
--%><polij:page>
    <polij:leftbar width="10%" >
		<polij:command>
			<polij:function>
				<polij:text><%=INTERNATIONALIZER.getText(CodiciBundle.JAF_CMD_INDIETRO)%></polij:text>
				<polij:href><%=lastActivityURL%>?evn_uscita=evento</polij:href>
			</polij:function>
		</polij:command>		
	</polij:leftbar>
	<polij:centerbar width="50%">
		<%
			String titolo = INTERNATIONALIZER.getText(CodiciBundle.JAF_TIT_PROFILO);
		%>
		<form name="" action="<polij:href><%=lastActivityURL%></polij:href>"
			method="post"><polij:recordSet title=""
			collectionId="polij_elenco_abilitazioni"
			msgEmpty="Nessuna abilitazione presente" type="button" >
				<polij:column title="<%=titolo%>" orderBy="xDescrizione">
					<polij:link>
						<polij:href><%=lastActivityURL%>?evn_scelta_profile=evento&profile=<polij:recordSetItem
								property="profile" />&d_profile=<polij:recordSetItem
								property="d_profile" />
						</polij:href>
						<polij:text>
							<polij:recordSetItem property="xDescrizione" />
						</polij:text>
					</polij:link>
				</polij:column>
			</polij:recordSet>
		</form>
	</polij:centerbar>
	<polij:rightbar width="40%"/>
</polij:page>
<%@page import="jaf.controller2.service.caller.DocManagerException.TipoErrore"%>
<%@page import="jaf.model2.util.JafUtil"%>
<%@page import="java.util.Date"%>
<%@page import="jaf.model2.util.DateUtil"%>
<%@page import="jaf.model2.exceptions.JafUnauthorizedException"%>
<%@page import="jaf.controller2.service.caller.DocManagerException"%>
<%@page import="jaf.model2.exceptions.JafRemoteServiceException"%>
<%@page contentType="text/html; charset=ISO-8859-1"%>
<%@page isErrorPage="true"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Error</title>		
		
		<script type='text/javascript'>var jaf_js_LANG = 'IT';</script>
		<link rel="stylesheet" type="text/css" href="//xqueries.test.polimi.it/webcommons/assets/ateneo2014.css.jsp?v=4" />
		<link rel='stylesheet' href='https://xqueries.test.polimi.it/webcommons/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css' type='text/css' media='screen' />
		<script src='https://xqueries.test.polimi.it/webcommons/ajax/libs/jqueryui/1.10.4/ui.js.jsp?lang=IT&amp;tab=yes'></script> 
		<script type="text/javascript">
		 if (typeof jQuery == 'undefined' || typeof jQuery === undefined) {  
		    alert('jQuery non � stato caricato: controlla di trustare il server https://xqueries.test.polimi.it/webcommons !(in sviluppo i certificati non sono trusted)');  
		 } 
		</script>
		<script type="text/javascript">
		   $.ajaxSetup ({ /* non mettere in cache le richieste AJAX (server per IE6 e IE7) */
		      cache: false
		   });
		</script>


		<style type="text/css">
			.div-corpo-messaggio{
				width:100%;
				font-size: 20pt;
				text-align: center;
			}
			.BoxInfoCard td div{
				padding: 50px 0;
			}
		</style>
	</head>
	<body>
        <table id="poliheader" >
			<tr>
				<td class="jaf-text-align-left td-logo-polimi" >
					<img class="logo-polimi" alt="logo-polimi" src="//xqueries.polimi.it/webcommons/assets/logo_polimi_testo.png">
				</td>
			</tr>
		</table>
		<div id="policontext"></div>
        <div class="Message WarningMessage"  id='mainErrore' style="width: 96%; marin: auto">
			<table style='border-spacing: 0;border-collapse: collapse;'>
				<tbody>
					<tr>
						<td class="TitleInfoCard">Error</td>
					</tr>
				</tbody>
			</table>
			<table  class="BoxInfoCard" style='border-spacing: 0;border-collapse: collapse;'>
				<tbody>
					<tr>
						<td class="" rowspan="1" colspan="1" >
							<div class="div-corpo-messaggio">
							 <%								
								String errorToShow = "401 - Access Denied";
								response.setStatus(401);
								out.println(errorToShow);
							%>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/>
		<table class="polifooter">
			<tfoot class="tfoot-tablePage">
		    	<tr>
		    		<td >
		    			<div class="jaf-text-align-left">
			   			</div>
		    		</td>
		    		<td>
		    			<div class="jaf-text-align-center">
							<%=JafUtil.nullToEmpty(System.getProperty("polij_footer_descrizione_area"))%>
		    			</div>
		    		</td>
		    		<td>
		    			<div class="jaf-text-align-right" id="div-tfoot-tablePage-centerBar">
		    				<%=DateUtil.getDate(new Date(System.currentTimeMillis()))%>
		    			</div>
		    		</td>
			   	</tr>
		    </tfoot>
		</table>
	</body>
</html>
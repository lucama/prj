<%@ taglib uri="http://www.ceda.polimi.it/taglibs/jaftemplate2-1.0" prefix="template" %><%--
--%><%@ page contentType="text/html;charset=ISO-8859-1" %><%--
--%><%
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	response.setDateHeader("Expires", 0); // Proxies.
%><%--
--%><template:insert parameter="message" defaultPath="/standard/message.jsp"/><%--
--%><template:insert parameter="body"/>
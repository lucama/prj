<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %><%--
--%><table id="poliheader" >
	<tr>
		<td class="jaf-text-align-left td-logo-polimi" >
			<a href="http://www.polimi.it" >
				<img class="logo-polimi" alt="logo-polimi" src="//xqueries.polimi.it/webcommons/assets/logo_polimi_testo.png" style="border: 0px none"/>
			</a>
			<div id="loading">
				<strong>Loading... <img src='//xqueries.polimi.it/webcommons/assets/loading.gif' alt="" /></strong>
			</div>
		</td>
            <td class="jaf-text-align-center info-DB" >
		        <c:catch var="errorInJsp">
                	<polij:infodb/>
		        </c:catch>
            </td>
			<td class="jaf-text-align-right info-utente" >
				<c:catch var="errorInJsp">            
					<polij:utenteInfo/>
				</c:catch>
            </td>
		</tr>
</table>